# FOSSIL SCM Server

This docker image will run fossil (http://www.fossil-scm.org) in server mode. 

## About the image

 * Docker images are tagged fossil:$VERSION (fossil:2.14, fossil:trunk, 
etc.) which correspond to a tag in fossil. 
 * Fossil is built from source code in a build image, then copied to the 
``fossil'' image.
 * Fossil is run as the ``fossil''.
 * Fossil repository files are in ``/opt/fossil'' and this labeled as a
 volume in the docker image.
 * Fossil server is run on port 8081 and port 8081 is EXPOSE'd in the
 Dockerfile.
 * Fossil CMD in Dockerfile is:
```
CMD /usr/bin/fossil server --repolist --port 8081 /opt/fossil
```

## Pushing images

Running make reg-all will build images and push them to
matkovach/fossil.

## Building

While we use a two stage build, fossil is build it's own build image and
then the fossil image copies fossil from it, the build is pretty simple.

```
docker build -t fossil:trunk --build-arg FOSSIL_INSTALL_VERSION=trunk -f
Dockerfile .
```

A make file is includes that will build trunk and two previous versions.

## Running with the Fossil

To run the image:

```
docker run --rm -d -p 8081:8080 -v /home/mek/fossils:/opt/fossil
fossil:trunk
```

This will run the fossil:trunk image, using /home/mek/fossils for the
/opt/fossil volume  (-v /home/mek/fossils:/opt/fossil) in the container. 

Port 8080 will to connect to back to the docker container port 8081 (-p 8081:8080). 

The container will run in the background (-d).

The container will be deleted when stopped (--rm).

## Notes

This will use the fossil as the web server. SSL is enabled in the fossil
build, but no work was done to enable SSL on the fossil server. Fossil's 
wiki provides info about using SSL,
https://www.fossil-scm.org/home/doc/trunk/www/ssl.wiki.

You can also run NGINX as your SSL endpoint and proxy NGINX back to the
fossil server.  
