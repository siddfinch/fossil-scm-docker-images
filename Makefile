NAME=fossil
ALL=build-trunk build-2.14 build-2.13
REGALL=reg-build-trunk reg-build-2.14 reg-build-2.13

default:
	@echo build-[version] -- build docker image $(NAME):[version]
	@echo all             -- build these targets: $(ALL)

build-%:
	docker build --tag $(NAME):$* --build-arg FOSSIL_INSTALL_VERSION=$* -f Dockerfile .

reg-build-%:
	docker build --tag matkovach/$(NAME):$* --build-arg FOSSIL_INSTALL_VERSION=$* -f Dockerfile .
	docker push matkovach/$(NAME):$* 
	
all: $(ALL)

reg-all: $(REGALL)

clean:
	@rm -rf *~
