FROM debian:stable as fossilbuild

ARG DEBIAN_FRONTNED=noninteractive
ARG FOSSIL_INSTALL_VERSION=trunk

RUN echo ${FOSSIL_INSTALL_VERSION}
RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install zlib1g-dev libssl-dev tcl tcl-dev curl tar -y
RUN curl "https://fossil-scm.org/home/tarball/fossil-src.tar.gz?name=fossil-src&tag=${FOSSIL_INSTALL_VERSION}" | tar zx
RUN cd fossil-src && ./configure --disable-fusefs --json --with-th1-docs --with-th1-hooks --with-tcl=1 --with-tcl-stubs --with-tcl-private-stubs
RUN cd fossil-src/src && mv main.c main.c.orig && sed s/\"now\"/0/ <main.c.orig >main.c
RUN cd fossil-src && make && strip fossil && cp fossil /usr/bin && chmod a+rx /usr/bin/fossil

FROM debian:stable
ARG DEBIAN_FRONTNED=noninteractive
ARG FOSSIL_INSTALL_VERSION=trunk
RUN echo ${FOSSIL_INSTALL_VERSION}
ENV HOME /opt/fossil
RUN apt-get update -y &&   \
    apt-get upgrade -y &&  \
    apt-get install tcl zlib1g libssl1.1 -y
RUN apt-get clean autoclean && apt-get autoremove -y && rm -rf /var/lib/{apt,dpkg,cache,log}/
RUN useradd -c "Fossil SCM Account" -M -s /bin/bash fossil
RUN mkdir -p /opt/fossil && chown fossil:fossil /opt/fossil
COPY --from=fossilbuild /usr/bin/fossil /usr/bin/fossil
RUN chown root:root /usr/bin/fossil && chmod 0555 /usr/bin/fossil
USER fossil
WORKDIR /opt/fossil
VOLUME [ "/opt/fossil" ]
EXPOSE 8081
CMD /usr/bin/fossil server --repolist --port 8081 /opt/fossil
